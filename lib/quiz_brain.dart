import 'dart:ffi';
import 'package:flutter/material.dart';
import 'question.dart';

class QuizBrain {
  int _questionNumber = 0;
  List<QuizObject> _questionBank = [
    QuizObject(question: 'You can lead a cow down stairs but not up stairs.', answer: false),
    QuizObject(question: 'Approximately one quarter of human bones are in the feet.', answer: true),
    QuizObject(question: 'A slug\'s blood is green.', answer: true),
    QuizObject(question: 'Buzz Aldrin\'s mother\'s maiden name was \"Moon\".', answer: true),
    QuizObject(question: 'It is illegal to pee in the Ocean in Portugal.', answer: true),
    QuizObject(question: 'No piece of square dry paper can be folded in half more than 7 times.', answer: false),
  ];

  int getNumberOfQuestions() {
    return _questionBank.length;
  }

  String getQuestionText() {
    return _questionBank[_questionNumber].question;
  }

  bool getQuestionAnswer() {
    return _questionBank[_questionNumber].answer;
  }

  void nextQuestion() {
    print(_questionBank.length);
    if (_questionNumber < _questionBank.length - 1) {
      _questionNumber++;
    }
  }

  void reset() {
    _questionNumber = 0;
  }

  bool isFinished() {
    if(_questionNumber == _questionBank.length - 1) {
      print('Now returning true');
      return true;
    } else {
      print('Now returning false');
      return false;
    }
  }
}